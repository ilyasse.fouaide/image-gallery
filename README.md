# Image gallery

Mini project with React + Vite + TailwindCSS.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js: [Link to Node.js](https://nodejs.org/)

## Installation

First, clone this repository:

1. Clone this repository

```
git clone https://gitlab.com/ilyasse.fouaide/image-gallery.git
```

2. Navigate to the project folder:

```
cd image-gallery
```

3. Install Dependencies

```
npm install
```

4. Run the app

```
npm run dev
```

## Usage
Once the app is running, open your web browser and navigate to http://localhost:5173.


## Demo

You can access the live demo of this project [here](https://image-gallery-ashy-chi.vercel.app/).
